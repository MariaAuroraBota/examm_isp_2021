
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class Ex2 extends JFrame{
    JTextArea tArea;
    JTextField tText;
    JButton bPublish;
    static final String filename="D:\\Uni\\Sem2\\SE\\Examen\\examm_isp_2021\\src\\TextFile.txt";

    Ex2()
    {
        setTitle("Exercise 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,400);
        setVisible(true);

    }
    public void init()
    {
        this.setLayout(null);
        int width=80;int height = 20;

        bPublish = new JButton("Submit");
        bPublish.setBounds(30,300,width, height);

        bPublish.addActionListener(new TreatButtonSubmit());

        tArea = new JTextArea();
        tArea.setBounds(20,140,150,100);


        add(tArea);add(bPublish);
    }
    public static void main(String[] args) {
        try
        {
            File file=new File(filename);
            if(file.createNewFile())
            {
                System.out.println("The file has been created");

            }
            else System.out.println("File is ready");
            new Ex2();
        }
        catch (IOException e)
        {
            System.out.println("WARNING!The file couldn't be created");
            e.printStackTrace();
        }

    }

    class TreatButtonSubmit implements ActionListener {
        public void actionPerformed(ActionEvent e)
        {
            String line=tArea.getText();
            line=line+"\n";
            try {
                BufferedWriter wr=new BufferedWriter(new FileWriter(filename,true));
                wr.write(line);
                wr.close();
            }
            catch (IOException a)
            {
                System.out.println("Error");
                a.printStackTrace();
            }
            tArea.setText("");
        }
    }
}
